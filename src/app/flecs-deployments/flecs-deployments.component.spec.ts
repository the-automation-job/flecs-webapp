import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlecsDeploymentsComponent } from './flecs-deployments.component';

describe('FlecsDeploymentsComponent', () => {
  let component: FlecsDeploymentsComponent;
  let fixture: ComponentFixture<FlecsDeploymentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlecsDeploymentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlecsDeploymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
