import { Component, OnInit } from '@angular/core';

export enum STATUS {
  UNKNOWN,
  DEPLOYED,
  UNDEPLOYED
}

export interface IDeployment {
  status: STATUS,
  app: string,
  manufacturer: string,
  version: string,
  image: string
}
@Component({
  selector: 'app-flecs-deployments',
  templateUrl: './flecs-deployments.component.html',
  styleUrls: ['./flecs-deployments.component.scss']
})
export class FlecsDeploymentsComponent implements OnInit {
  STATUS = STATUS;
  displayedColumns = ['status', 'app', 'manufacturer', 'version', 'actions'];

  fakeData: IDeployment[] = [
    {
      status: STATUS.DEPLOYED,
      app: 'CODESYS Runtime',
      manufacturer: 'CODESYS Group',
      version: 'V4.1.0.0',
      image: "/assets/linux_sl.png"
    },
    {
      status: STATUS.UNDEPLOYED,
      app: 'CODESYS Edge Gateway',
      manufacturer: 'CODESYS Group',
      version: 'V4.1.0.0',
      image: "/assets/edge.png"
    },
    {
      status: STATUS.UNKNOWN,
      app: 'Grafana',
      manufacturer: 'Grafana Labs',
      version: '8.0.6',
      image: "/assets/grafana.png"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
