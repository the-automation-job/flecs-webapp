import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlecsMarketplaceComponent } from './flecs-marketplace.component';

describe('FlecsMarketplaceComponent', () => {
  let component: FlecsMarketplaceComponent;
  let fixture: ComponentFixture<FlecsMarketplaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlecsMarketplaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlecsMarketplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
