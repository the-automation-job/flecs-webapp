import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { FlecsSidenavComponent } from './flecs-sidenav/flecs-sidenav.component';
import { FlecsDeploymentsComponent } from './flecs-deployments/flecs-deployments.component';
import { FlecsMarketplaceComponent } from './flecs-marketplace/flecs-marketplace.component';
import { FlecsContentComponent } from './flecs-content/flecs-content.component';
import { MatTableModule } from '@angular/material/table';
import {FlecsFrameComponent} from './flecs-frame/flecs-frame.component';

@NgModule({
  declarations: [
    AppComponent,
    FlecsFrameComponent,
    FlecsSidenavComponent,
    FlecsDeploymentsComponent,
    FlecsMarketplaceComponent,
    FlecsContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatChipsModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
