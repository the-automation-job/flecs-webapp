import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-flecs-sidenav',
  templateUrl: './flecs-sidenav.component.html',
  styleUrls: ['./flecs-sidenav.component.scss']
})
export class FlecsSidenavComponent implements OnInit {


  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
