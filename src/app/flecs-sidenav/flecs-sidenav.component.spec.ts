import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlecsSidenavComponent } from './flecs-sidenav.component';

describe('FlecsSidenavComponent', () => {
  let component: FlecsSidenavComponent;
  let fixture: ComponentFixture<FlecsSidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlecsSidenavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlecsSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
