import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlecsContentComponent } from './flecs-content.component';

describe('FlecsContentComponent', () => {
  let component: FlecsContentComponent;
  let fixture: ComponentFixture<FlecsContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlecsContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlecsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
