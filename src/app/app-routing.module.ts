import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FlecsDeploymentsComponent} from './flecs-deployments/flecs-deployments.component';
import {FlecsMarketplaceComponent} from './flecs-marketplace/flecs-marketplace.component';

const routes: Routes = [
  {path:"deployments", component: FlecsDeploymentsComponent},
  {path:"marketplace", component: FlecsMarketplaceComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
